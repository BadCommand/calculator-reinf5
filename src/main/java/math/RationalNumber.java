package math;

final class RationalNumber {

	private int numerator;
	private int denominator;
	String result;

	public RationalNumber(int num, int denom) {

		if (denom == 0)
			throw new IllegalArgumentException();

		if (denom < 0) {
			num = num * -1;
			denom = denom * -1;
		}

		this.numerator = num;
		this.denominator = denom;

		reduce();
	}

	public RationalNumber(int num) {

		this.numerator = num;
		this.denominator = 1;

	}

	private void reduce() {

		if (numerator != 0) {
			int common = greatesCommonDivisor(Math.abs(numerator), denominator);

			numerator = numerator / common;
			denominator = denominator / common;
		}
	}

	private int greatesCommonDivisor(int numb1, int numb2) {

		while (numb1 != numb2)
			if (numb1 > numb2)
				numb1 = numb1 - numb2;
			else
				numb2 = numb2 - numb1;

		return numb1;
	}

	public int getNumerator() {

		return numerator;
	}

	public int getDenominator() {

		return denominator;
	}

	public RationalNumber reciprocal() {

		return new RationalNumber(denominator, numerator);
	}

	public RationalNumber negative() {

		int num = this.numerator * -1;
		int denom = this.denominator;

		return new RationalNumber(num, denom);
	}

	public RationalNumber add(RationalNumber numb) {

		int commonDenominator = denominator * numb.getDenominator();
		int numerator1 = numerator * numb.getDenominator();
		int numerator2 = numb.getNumerator() * denominator;
		int sum = numerator1 + numerator2;

		return new RationalNumber(sum, commonDenominator);
	}

	public RationalNumber subtract(RationalNumber numb) {

		int commonDenominator = denominator * numb.getDenominator();
		int numerator1 = numerator * numb.getDenominator();
		int numerator2 = numb.getNumerator() * denominator;
		int difference = numerator1 - numerator2;

		return new RationalNumber(difference, commonDenominator);
	}

	public RationalNumber multiply(RationalNumber numb) {

		int numer = numerator * numb.getNumerator();
		int denom = denominator * numb.getDenominator();

		return new RationalNumber(numer, denom);
	}

	public RationalNumber divide(RationalNumber numb) {

		return multiply(numb.reciprocal());
	}

	public String toString() {

		if (numerator == 0)
			result = "0";
		else if (denominator == 1)
			result = numerator + "";
		else
			result = numerator + "/" + denominator;

		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + denominator;
		result = prime * result + numerator;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RationalNumber other = (RationalNumber) obj;
		if (denominator != other.denominator)
			return false;
		if (numerator != other.numerator)
			return false;
		return true;
	}
}
