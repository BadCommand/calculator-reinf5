/*
 * Project and Training 1 - Computer Science, Berner Fachhochschule
 */

package math;

import java.util.Scanner;

class RationalCalculator {

	public static RationalNumber convert(String number) {

		int ind = number.indexOf('/');
		if (ind > 0) {
			int num = Integer.parseInt(number.substring(0, ind));
			int denom = Integer.parseInt(number.substring(ind + 1, number.length()));
			return new RationalNumber(num, denom);
		} else {
			return new RationalNumber(Integer.parseInt(number));
		}
	}

	public static RationalNumber evaluate(String number) {

		Scanner scan = new Scanner(number);
		RationalNumber newRN;
		RationalNumber result = new RationalNumber(0);
		String operator = "";

		while (scan.hasNext("-?[0-9]+(/-?[0-9]+)?") || scan.hasNext("[+\\-*:]")) {
			if (scan.hasNext("-?[0-9]+(/-?[0-9]+)?")) {
				result = RationalCalculator.convert(scan.next("-?[0-9]+(/-?[0-9]+)?"));
			} else {
				operator = scan.next("[+\\-*:]");
				newRN = RationalCalculator.convert(scan.next("-?[0-9]+(/-?[0-9]+)?"));

				switch (operator) {
				case "+":
					result = result.add(newRN);
					break;
				case "-":
					result = result.subtract(newRN);
					break;
				case ":":
					result = result.divide(newRN);
					break;
				case "*":
					result = result.multiply(newRN);
				}
			}
		}
		scan.close();

		return result;

	}
}
